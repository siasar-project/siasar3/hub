lint:
	docker compose run --rm data-hub npm run lint

fix:
	docker compose run --rm data-hub npm run lint:fix

run:
	docker compose run --rm data-hub

export:
	docker compose run --rm data-hub npm run export

db:
	psql postgres://data_hub:data_hub@localhost:5433/data_hub

server:
	docker compose down
	docker compose up

build_etl:
	docker compose build data-hub

build_grafana:
	docker compose build grafana

build: build_etl build_grafana