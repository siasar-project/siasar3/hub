import pg from "pg";
import fs from "fs";

export default class Output {
  constructor(config) {
    this.pool = new pg.Pool(config);
  }

  query(query) {
    return this.pool.query(query);
  }

  end() {
    return this.pool.end();
  }

  createSchema() {
    return this.query(fs.readFileSync("schema.sql", "utf8"));
  }

  insertCountries(rows) {
    if (!rows.length) return;

    return this.query(`
      INSERT INTO countries (
        code,
        name,
        fullname,
        geom,
        enabled
      )
      VALUES ${rows
        .map(
          (row) => `(
            '${row.code}',
            '${row.name}',
            '${row.fullname}',
            ST_Multi(ST_GeomFromGeoJSON('${JSON.stringify(row.geom)}')),
            ${row.enabled}
          )`,
        )
        .join(",")}
      ON CONFLICT DO NOTHING;
    `);
  }

  insertPoints(rows) {
    if (!rows.length) return;

    return this.query(`
      INSERT INTO points (
        id,
        status,
        version,
        country_code
      )
      VALUES ${rows
        .map(
          (row) => `(
            '${row.ulid}',
            '${row.status}',
            '${row.version}',
            '${row.country_code}'
          )`,
        )
        .join(",")}
      ON CONFLICT DO NOTHING;
    `);
  }

  insertCommunities(rows) {
    if (!rows.length) return;

    const indicators = [
      "Cwater",
      "WSLACC",
      "WSLCON",
      "WSLSEA",
      "WSLQUA",
      "WSL",
      "HHflushToilet",
      "HHdryPit",
      "HHcompost",
      "SHLSSL",
      "SHLODL",
      "SHLSSP",
      "Fhygiene",
      "SHLHHP",
      "SHL",
      "WSICom",
      "SEPCom",
      "WSP",
    ];

    return this.query(`
      INSERT INTO communities (
        id,
        point_id,
        name,
        latitude,
        longitude,
        altitude,
        geom,
        status,
        version,
        score,
        ${indicators.map((i) => i.toLowerCase()).join(",")},
        image_url,
        population,
        households,
        households_without_water,
        num_systems,
        num_providers,
        no_facility_number,
        country_code,
        country,
        adm1,
        adm2,
        adm3,
        adm4
      )
      VALUES ${rows
        .map(
          (row) => `(
            '${row.ulid}',
            '${row.pointUlid}',
            '${row.name}',
            ${row.latitude},
            ${row.longitude},
            ${row.altitude},
            ST_SetSRID(ST_POINT(${row.longitude}, ${row.latitude}), 4326),
            '${row.status}',
            '${row.version}',
            '${row.indicators.find((i) => i.name === "WSP")?.label || "E"}',
            ${indicators.map((i) => row.indicators.find((ri) => ri.name === i)?.value ?? "null").join(",")},
            ${row.images.length ? `'${row.images[0]}'` : null},
            ${row.population},
            ${row.households},
            ${row.households_without_water},
            ${row.entities_count?.num_systems ?? null},
            ${row.entities_count?.num_providers ?? null},
            ${row.no_facility_number},
            '${row.country_code}',
            '${row.country}',
            '${row.adm1}',
            '${row.adm2}',
            '${row.adm3}',
            '${row.adm4}'
          )`,
        )
        .join(",")}
      ON CONFLICT DO NOTHING;
    `);
  }

  insertSystems(rows) {
    if (!rows.length) return;

    const indicators = [
      "FOAM",
      "FclRes",
      "WSIAUT",
      "Fdist",
      "WSIINF",
      "WSIPRO",
      "WSITRE",
      "WSI",
      "Fintk",
      "Ftran",
      "Fstor",
    ];

    return this.query(`
      INSERT INTO systems (
        id,
        point_id,
        name,
        latitude,
        longitude,
        altitude,
        num_communities,
        num_providers,
        served_households,
        system_type,
        geom,
        status,
        version,
        score,
        ${indicators.map((i) => i.toLowerCase()).join(",")},
        image_url,
        country_code,
        country,
        adm1,
        adm2,
        adm3,
        adm4
      )
      VALUES ${rows
        .map(
          (row) => `(
            '${row.ulid}',
            '${row.pointUlid}',
            '${row.name}',
            ${row.latitude},
            ${row.longitude},
            ${row.altitude},
            ${row.entities_count?.num_communities ?? null},
            ${row.entities_count?.num_providers ?? null},
            ${row.entities_count?.served_households ?? null},
            '${row.system_type}',
            ST_SetSRID(ST_POINT(${row.longitude}, ${row.latitude}), 4326),
            '${row.status}',
            '${row.version}',
            '${row.indicators.find((i) => i.name === "WSI")?.label || "E"}',
            ${indicators
              .map((i) => {
                return row.indicators.find((ri) => ri.name === i)?.value ?? "null";
              })
              .join(",")},
            ${row.images.length ? `'${row.images[0]}'` : null},
            '${row.country_code}',
            '${row.country}',
            '${row.adm1}',
            '${row.adm2}',
            '${row.adm3}',
            '${row.adm4}'
          )`,
        )
        .join(",")}
      ON CONFLICT DO NOTHING;
    `);
  }

  insertProviders(rows) {
    if (!rows.length) return;

    const indicators = [
      "FMPS",
      "FoperPSmeet",
      "FoperPSboard",
      "FoperPSregl",
      "FoperPS",
      "FgenPS",
      "FtransPS",
      "SEPORG",
      "FO&M",
      "SEPOPM",
      "Frec",
      "Frent",
      "SEPECO",
      "FhygPro",
      "FenvSan",
      "SEPENV",
      "SEP",
    ];

    return this.query(`
      INSERT INTO providers (
        id,
        point_id,
        name,
        latitude,
        longitude,
        altitude,
        provider_type,
        num_systems,
        geom,
        status,
        version,
        score,
        ${indicators.map((i) => i.toLowerCase().replace("&", "")).join(",")},
        image_url,
        country_code,
        country,
        adm1,
        adm2,
        adm3,
        adm4
      )
      VALUES ${rows
        .map(
          (row) => `(
            '${row.ulid}',
            '${row.pointUlid}',
            '${row.name}',
            '${row.latitude}',
            '${row.longitude}',
            '${row.altitude}',
            '${row.provider_type}',
            ${row.entities_count?.num_systems ?? null},
            ST_SetSRID(ST_POINT(${row.longitude}, ${row.latitude}), 4326),
            '${row.status}',
            '${row.version}',
            '${row.indicators.find((i) => i.name === "SEP")?.label || "E"}',
            ${indicators.map((i) => row.indicators.find((ri) => ri.name === i)?.value ?? "null").join(",")},
            ${row.images.length ? `'${row.images[0]}'` : null},
            '${row.country_code}',
            '${row.country}',
            '${row.adm1}',
            '${row.adm2}',
            '${row.adm3}',
            '${row.adm4}'
          )`,
        )
        .join(",")}
      ON CONFLICT DO NOTHING;
    `);
  }

  insertSchools(rows) {
    if (!rows.length) return;

    const indicators = ["SHCSWA", "ShcSan", "ShcHyg", "SHCSSH", "SHC"];

    return this.query(`
      INSERT INTO schools (
        id,
        name,
        code,
        latitude,
        longitude,
        altitude,
        school_type,
        geom,
        status,
        version,
        have_water_supply_system,
        score,
        ${indicators.map((i) => i.toLowerCase()).join(",")},
        staff_women,
        staff_men,
        students_female,
        students_male,
        have_toilets,
        image_url,
        country_code,
        country,
        adm1,
        adm2,
        adm3,
        adm4
      )
      VALUES ${rows
        .map(
          (row) => `(
            '${row.ulid}',
            '${row.name}',
            '${row.code}',
            '${row.latitude}',
            '${row.longitude}',
            '${row.altitude}',
            '${row.school_type}',
            ST_SetSRID(ST_POINT(${row.longitude}, ${row.latitude}), 4326),
            '${row.status}',
            '${row.version}',
            '${row.have_water_supply_system}',
            '${row.indicators.find((i) => i.name === "SHC")?.label || "E"}',
            ${indicators.map((i) => row.indicators.find((ri) => ri.name === i)?.value ?? "null").join(",")},
            ${row.staff_women},
            ${row.staff_men},
            ${row.students_female},
            ${row.students_male},
            ${row.have_toilets},
            ${row.images.length ? `'${row.images[0]}'` : null},
            '${row.country_code}',
            '${row.country}',
            '${row.adm1}',
            '${row.adm2}',
            '${row.adm3}',
            '${row.adm4}'
          )`,
        )
        .join(",")}
      ON CONFLICT DO NOTHING;
    `);
  }

  insertCommunitiesSystems(rows) {
    if (!rows.length) return;

    const tableName = `tmp_${Math.round(Math.random() * 100000)}`;

    return this.query(`
      CREATE TEMP TABLE ${tableName} AS
      SELECT
          community_id,
          system_id,
          provider_id,
          served_households
      FROM
          (VALUES ${rows
            .map(
              (row) => `(
                  '${row.community_id}',
                  '${row.system_id}',
                  '${row.provider_id}',
                  ${row.served_households}
                )`,
            )
            .join(",")}
          ) AS data(community_id, system_id, provider_id, served_households);

      INSERT INTO communities_systems_providers
      (
        SELECT t.*
        FROM ${tableName} t
        JOIN communities c ON c.id = t.community_id
        JOIN systems s ON s.id = t.system_id
        JOIN providers p ON p.id = t.provider_id
      );
    `);
  }

  insertCommunitiesSchools(rows) {
    if (!rows.length) return;

    const tableName = `tmp_${Math.round(Math.random() * 100000)}`;

    return this.query(`
      CREATE TEMP TABLE ${tableName} AS
      SELECT
          community_id,
          school_id
      FROM
        (VALUES ${rows
          .map(
            (row) => `(
                '${row.community_id}',
                '${row.school_id}'
            )`,
          )
          .join(",")}
        ) AS data(community_id, school_id);

      INSERT INTO communities_schools
      (
        SELECT t.*
        FROM ${tableName} t
        JOIN communities c ON c.id = t.community_id
        JOIN schools s ON s.id = t.school_id
      );
    `);
  }

  insertHealthCenters(rows) {
    if (!rows.length) return;

    const indicators = ["HCCHWA", "HccSan", "HccHyg", "HCCHSH", "HCC"];

    return this.query(`
      INSERT INTO health_centers (
        id,
        name,
        code,
        latitude,
        longitude,
        altitude,
        center_type,
        geom,
        status,
        version,
        score,
        ${indicators.map((i) => i.toLowerCase()).join(",")},
        staff_women,
        staff_men,
        avg_female_patients,
        avg_male_patients,
        have_water_supply,
        have_toilets,
        image_url,
        country_code,
        country,
        adm1,
        adm2,
        adm3,
        adm4
      )
      VALUES ${rows
        .map(
          (row) => `(
            '${row.ulid}',
            '${row.name}',
            '${row.code}',
            '${row.latitude}',
            '${row.longitude}',
            '${row.altitude}',
            '${row.center_type}',
            ST_SetSRID(ST_POINT(${row.longitude}, ${row.latitude}), 4326),
            '${row.status}',
            '${row.version}',
            '${row.indicators.find((i) => i.name === "HCC")?.label || "E"}',
           ${indicators.map((i) => row.indicators.find((ri) => ri.name === i)?.value ?? "null").join(",")},
            ${row.staff_women},
            ${row.staff_men},
            ${row.avg_female_patients},
            ${row.avg_male_patients},
            '${row.have_water_supply}',
            ${row.have_toilets},
            ${row.images.length ? `'${row.images[0]}'` : null},
            '${row.country_code}',
            '${row.country}',
            '${row.adm1}',
            '${row.adm2}',
            '${row.adm3}',
            '${row.adm4}'
          )`,
        )
        .join(",")}
      ON CONFLICT DO NOTHING;
    `);
  }

  insertCommunitiesHealthCenters(rows) {
    if (!rows.length) return;

    const tableName = `tmp_${Math.round(Math.random() * 100000)}`;

    return this.query(`
      CREATE TEMP TABLE ${tableName} AS
      SELECT
          community_id,
          health_center_id
      FROM
        (VALUES ${rows
          .map(
            (row) => `(
                '${row.community_id}',
                '${row.health_center_id}'
              )`,
          )
          .join(",")}
        ) AS data(community_id, health_center_id);

      INSERT INTO communities_health_centers
      (
        SELECT t.*
        FROM ${tableName} t
        JOIN communities c ON c.id = t.community_id
        JOIN health_centers hc ON hc.id = t.health_center_id
      );
    `);
  }

  insertTechnicalAssistanceProviders(rows) {
    if (!rows.length) return;

    const indicators = ["PATSIN", "PATCAP", "PATCOB", "PATINT", "PAT"];

    return this.query(`
      INSERT INTO technical_assistance_providers (
        id,
        version,
        name,
        country,
        adm1,
        adm2,
        adm3,
        adm4,
        latitude,
        longitude,
        altitude,
        type_tap,
        phase_provide_tap,
        tap_should_assist,
        ${indicators.map((i) => i.toLowerCase()).join(",")},
        score,
        country_code,
        status,
        image_url,
        code,
        geom,
        service_providers_supported_12m
      )
      VALUES ${rows
        .map(
          (row) => `(
            '${row.ulid}',
            '${row.version}',
            '${row.name}',
            '${row.country}',
            '${row.adm1}',
            '${row.adm2}',
            '${row.adm3}',
            '${row.adm4}',
            '${row.latitude}',
            '${row.longitude}',
            '${row.altitude}',
            '${row.type_tap}',
            '${row.phase_provide_tap}',
            '${row.tap_should_assist}',
           ${indicators.map((i) => row.indicators.find((ri) => ri.name === i)?.value ?? "null").join(",")},
            '${row.indicators.find((i) => i.name === "PAT")?.label || "E"}',
            '${row.country_code}',
            '${row.status}',
            ${row.images.length ? `'${row.images[0]}'` : null},
            '${row.code}',
            ST_SetSRID(ST_POINT(${row.longitude}, ${row.latitude}), 4326),
            '${row.service_providers_supported_12m}'
          )`,
        )
        .join(",")}
      ON CONFLICT DO NOTHING;
    `);
  }
}
