FROM grafana/grafana-oss:11.1.0

USER root

##################################################################
## HANDS-ON
## Update HTML, INI files
##################################################################

# Update Title
RUN sed -i 's|<title>\[\[.AppTitle\]\]</title>|<title>SIASAR Hub</title>|g' /usr/share/grafana/public/views/index.html
RUN sed -i 's|Loading Grafana|Loading SIASAR Hub|g' /usr/share/grafana/public/views/index.html

## Update Mega and Help menu
RUN sed -i "s|\[\[.NavTree\]\],|nav,|g; \
    s|window.grafanaBootData = {| \
    let nav = [[.NavTree]]; \
    const alerting = nav.find((element) => element.id === 'alerting'); \
    if (alerting) { alerting['url'] = '/alerting/list'; } \
    const dashboards = nav.find((element) => element.id === 'dashboards/browse'); \
    if (dashboards) { dashboards['children'] = [];} \
    const connections = nav.find((element) => element.id === 'connections'); \
    if (connections) { connections['url'] = '/datasources'; connections['children'].shift(); } \
    const help = nav.find((element) => element.id === 'help'); \
    if (help) { help['subTitle'] = 'SIASAR Hub'; help['children'] = [];} \
    window.grafanaBootData = {|g" \
    /usr/share/grafana/public/views/index.html

# Move SIASAR Hub App to navigation root section
RUN sed -i 's|\[navigation.app_sections\]|\[navigation.app_sections\]\nvolkovlabs-app=root|g' /usr/share/grafana/conf/defaults.ini

##################################################################
## HANDS-ON
## Update JavaScript files
##################################################################

RUN find /usr/share/grafana/public/build/ -name *.js \
## Update Title
  -exec sed -i 's|AppTitle="Grafana"|AppTitle="SIASAR Hub"|g' {} \; \
## Update Login Title
  -exec sed -i 's|LoginTitle="Welcome to Grafana"|LoginTitle="Welcom to SIASAR Hub"|g' {} \; \
## Remove Documentation, Support, Community in the Footer
  -exec sed -i 's|\[{target:"_blank",id:"documentation".*grafana_footer"}\]|\[\]|g' {} \; \
## Remove Edition in the Footer
  -exec sed -i 's|({target:"_blank",id:"license",.*licenseUrl})|()|g' {} \; \
## Remove Version in the Footer
  -exec sed -i 's|({target:"_blank",id:"version",.*CHANGELOG.md":void 0})|()|g' {} \; \
## Remove News icon
  -exec sed -i 's|..createElement(....,{className:.,onClick:.,iconOnly:!0,icon:"rss","aria-label":"News"})|null|g' {} \; \
## Remove Open Source icon
  -exec sed -i 's|.push({target:"_blank",id:"version",text:`${..edition}${.}`,url:..licenseUrl,icon:"external-link-alt"})||g' {} \;

##################################################################
## CLEANING
## Remove Native Data Sources
##################################################################

RUN rm -rf /usr/share/grafana/public/app/plugins/datasource/elasticsearch /usr/share/grafana/public/build/elasticsearch* \
  /usr/share/grafana/public/app/plugins/datasource/graphite /usr/share/grafana/public/build/graphite* \
  /usr/share/grafana/public/app/plugins/datasource/opentsdb /usr/share/grafana/public/build/opentsdb* \
  /usr/share/grafana/public/app/plugins/datasource/influxdb /usr/share/grafana/public/build/influxdb* \
  /usr/share/grafana/public/app/plugins/datasource/mssql /usr/share/grafana/public/build/mssql* \
  /usr/share/grafana/public/app/plugins/datasource/mysql /usr/share/grafana/public/build/mysql* \
  /usr/share/grafana/public/app/plugins/datasource/tempo /usr/share/grafana/public/build/tempo* \
  /usr/share/grafana/public/app/plugins/datasource/jaeger /usr/share/grafana/public/build/jaeger* \
  /usr/share/grafana/public/app/plugins/datasource/zipkin /usr/share/grafana/public/build/zipkin* \
  /usr/share/grafana/public/app/plugins/datasource/azuremonitor /usr/share/grafana/public/build/azureMonitor* \
  /usr/share/grafana/public/app/plugins/datasource/cloudwatch /usr/share/grafana/public/build/cloudwatch* \
  /usr/share/grafana/public/app/plugins/datasource/cloud-monitoring /usr/share/grafana/public/build/cloudMonitoring* \
  /usr/share/grafana/public/app/plugins/datasource/parca /usr/share/grafana/public/build/parca* \
  /usr/share/grafana/public/app/plugins/datasource/phlare /usr/share/grafana/public/build/phlare* \
  /usr/share/grafana/public/app/plugins/datasource/grafana-pyroscope-datasource /usr/share/grafana/public/build/pyroscope*

## Remove Cloud and Enterprise categories
RUN find /usr/share/grafana/public/build/ -name *.js \
  -exec sed -i 's|.id==="enterprise"|.id==="notanenterprise"|g' {} \; \
  -exec sed -i 's|.id==="cloud"|.id==="notacloud"|g' {} \;

##################################################################
## CLEANING
## Remove Native Panels
##################################################################

RUN rm -rf /usr/share/grafana/public/app/plugins/panel/alertlist \
  /usr/share/grafana/public/app/plugins/panel/annolist \
  /usr/share/grafana/public/app/plugins/panel/dashlist \
  /usr/share/grafana/public/app/plugins/panel/news \
  # /usr/share/grafana/public/app/plugins/panel/geomap \
  /usr/share/grafana/public/app/plugins/panel/table-old \
  /usr/share/grafana/public/app/plugins/panel/traces \
  /usr/share/grafana/public/app/plugins/panel/flamegraph

##################################################################

## Copy public files
COPY public /usr/share/grafana/public

## Set Grafana options
ENV GF_ENABLE_GZIP=true
ENV GF_USERS_DEFAULT_THEME=light
ENV GF_DEFAULT_INSTANCE_NAME=siasar

## Enable Anonymous Authentication
ENV GF_AUTH_ANONYMOUS_ENABLED=true
ENV GF_AUTH_BASIC_ENABLED=false
ENV GF_USERS_ALLOW_SIGN_UP=false

## Disable Help Section
ENV GF_HELP_ENABLED=false

## Disable Public Dashboards
ENV GF_PUBLIC_DASHBOARDS_ENABLED=false

## Enable Panels
ENV GF_PANELS_DISABLE_SANITIZE_HTML=true
ENV GF_PANELS_ENABLE_ALPHA=true

## Disable Updates Check
ENV GF_ANALYTICS_CHECK_FOR_UPDATES=false

## Paths
ENV GF_PATHS_PROVISIONING="/etc/grafana/provisioning"
ENV GF_PATHS_PLUGINS="/var/lib/grafana/plugins"
ENV GF_PATHS_DASHBOARDS="/etc/grafana/dashboards"

## Security
ENV GF_SECURITY_ALLOW_EMBEDDING=true

## Install plugins
ENV GF_INSTALL_PLUGINS=volkovlabs-echarts-panel,marcusolsson-json-datasource,orchestracities-map-panel,marcusolsson-dynamictext-panel,dalvany-image-panel,volkovlabs-variable-panel,gapit-htmlgraphics-panel

## Copy Provisioning
COPY --chown=grafana:root provisioning $GF_PATHS_PROVISIONING

## Copy Dashboards
COPY --chown=grafana:root dashboards $GF_PATHS_DASHBOARDS

## Set Home Dashboard
ENV GF_DASHBOARDS_DEFAULT_HOME_DASHBOARD_PATH=$GF_PATHS_DASHBOARDS/home.json
