DROP TABLE IF EXISTS points CASCADE;

CREATE TABLE points (
  id varchar(26) PRIMARY KEY,
  status text,
  version timestamp,
  country_code varchar(2)
);

CREATE INDEX points_country_code_idx ON points (country_code);

DROP TABLE IF EXISTS communities CASCADE;

CREATE TABLE communities (
  id varchar(26) PRIMARY KEY,
  point_id varchar(26) REFERENCES points(id) ON DELETE CASCADE,
  version timestamp,
  name text,
  country text,
  adm1 text,
  adm2 text,
  adm3 text,
  adm4 text,
  latitude float,
  longitude float,
  altitude float,
  population integer,
  households integer,
  households_without_water integer,
  cwater float,
  num_systems integer,
  num_providers integer,
  wslacc float,
  wslcon float,
  wslsea float,
  wslqua float,
  hhflushtoilet float,
  hhdrypit float,
  hhcompost float,
  shlssl float,
  shlodl float,
  shlssp float,
  fhygiene float,
  shlhhp float,
  wsl float,
  shl float,
  wsicom float,
  sepcom float,
  wsp float,
  score varchar(1),
  no_facility_number integer,
  image_url text,
  country_code varchar(2),
  status text,
  geom geometry(Point, 4326)
);

CREATE INDEX communities_score_idx ON communities (score);

CREATE INDEX communities_wsp_idx ON communities (wsp);

CREATE INDEX communities_country_code_idx ON communities (country_code);

CREATE INDEX communities_country_idx ON communities (country);

CREATE INDEX communities_adm1_idx ON communities (adm1);

CREATE INDEX communities_adm2_idx ON communities (adm2);

CREATE INDEX communities_adm3_idx ON communities (adm3);

CREATE INDEX communities_adm4_idx ON communities (adm4);

CREATE INDEX communities_geom_idx ON communities USING GIST (geom);

DROP TABLE IF EXISTS systems CASCADE;

CREATE TABLE systems (
  id varchar(26) PRIMARY KEY,
  point_id varchar(26) REFERENCES points(id) ON DELETE CASCADE,
  version timestamp,
  name text,
  country text,
  adm1 text,
  adm2 text,
  adm3 text,
  adm4 text,
  latitude float,
  longitude float,
  altitude float,
  num_communities integer,
  num_providers integer,
  system_type character varying,
  served_households integer,
  fintk float,
  ftran float,
  fstor float,
  fdist float,
  foam float,
  fclres float,
  wsiaut float,
  wsiinf float,
  wsipro float,
  wsitre float,
  wsi float,
  score varchar(1),
  image_url text,
  country_code varchar(2),
  geom geometry(Point, 4326),
  status text
);

CREATE INDEX systems_score_idx ON systems (score);

CREATE INDEX systems_wsi_idx ON systems (wsi);

CREATE INDEX systems_country_code_idx ON systems (country_code);

CREATE INDEX systems_country_idx ON systems (country);

CREATE INDEX systems_adm1_idx ON systems (adm1);

CREATE INDEX systems_adm2_idx ON systems (adm2);

CREATE INDEX systems_adm3_idx ON systems (adm3);

CREATE INDEX systems_adm4_idx ON systems (adm4);

CREATE INDEX systems_geom_idx ON systems USING GIST (geom);

DROP TABLE IF EXISTS providers CASCADE;

CREATE TABLE providers (
  id varchar(26) PRIMARY KEY,
  point_id varchar(26) REFERENCES points(id) ON DELETE CASCADE,
  version timestamp,
  name text,
  country text,
  adm1 text,
  adm2 text,
  adm3 text,
  adm4 text,
  latitude float,
  longitude float,
  altitude float,
  num_systems integer,
  provider_type character varying,
  foperpsmeet float,
  foperpsboard float,
  foperpsregl float,
  foperps float,
  fgenps float,
  ftransps float,
  fmps float,
  seporg float,
  fom float,
  sepopm float,
  frec float,
  frent float,
  sepeco float,
  fhygpro float,
  fenvsan float,
  sepenv float,
  sep float,
  score varchar(1),
  image_url text,
  country_code varchar(2),
  geom geometry(Point, 4326),
  status text
);

CREATE INDEX providers_score_idx ON providers (score);

CREATE INDEX providers_sep_idx ON providers (sep);

CREATE INDEX providers_country_code_idx ON providers (country_code);

CREATE INDEX providers_country_idx ON providers (country);

CREATE INDEX providers_adm1_idx ON providers (adm1);

CREATE INDEX providers_adm2_idx ON providers (adm2);

CREATE INDEX providers_adm3_idx ON providers (adm3);

CREATE INDEX providers_adm4_idx ON providers (adm4);

CREATE INDEX providers_geom_idx ON providers USING GIST (geom);

DROP TABLE IF EXISTS countries CASCADE;

CREATE TABLE countries (
  code varchar(2) PRIMARY KEY,
  name text,
  fullname text,
  geom geometry(MultiPolygon, 4326),
  enabled boolean
);

CREATE INDEX countries_geom_idx ON countries USING GIST (geom);

DROP TABLE IF EXISTS communities_systems_providers CASCADE;

CREATE TABLE communities_systems_providers (
  community_id varchar(26) REFERENCES communities(id) ON DELETE CASCADE,
  system_id varchar(26) REFERENCES systems(id) ON DELETE CASCADE,
  provider_id varchar(26) REFERENCES providers(id) ON DELETE CASCADE,
  served_households integer,
  CONSTRAINT unique_community_system_provider UNIQUE (community_id, system_id, provider_id)
);

CREATE INDEX communities_systems_providers_households_idx ON providers (sep);

DROP TABLE IF EXISTS schools CASCADE;

CREATE TABLE schools (
  id varchar(26) PRIMARY KEY,
  version timestamp,
  name text,
  country text,
  adm1 text,
  adm2 text,
  adm3 text,
  adm4 text,
  latitude float,
  longitude float,
  altitude float,
  school_type character varying,
  staff_women integer,
  staff_men integer,
  students_female integer,
  students_male integer,
  have_water_supply_system character varying,
  shcswa float,
  have_toilets boolean,
  shcsan float,
  shchyg float,
  shcssh float,
  shc float,
  score varchar(1),
  image_url text,
  country_code varchar(2),
  code text,
  geom geometry(Point, 4326),
  status text
);

CREATE INDEX schools_score_idx ON schools (score);

CREATE INDEX schools_shc_idx ON schools (shc);

CREATE INDEX schools_country_code_idx ON schools (country_code);

CREATE INDEX schools_country_idx ON schools (country);

CREATE INDEX schools_adm1_idx ON schools (adm1);

CREATE INDEX schools_adm2_idx ON schools (adm2);

CREATE INDEX schools_adm3_idx ON schools (adm3);

CREATE INDEX schools_adm4_idx ON schools (adm4);

CREATE INDEX schools_geom_idx ON schools USING GIST (geom);

DROP TABLE IF EXISTS communities_schools CASCADE;

CREATE TABLE communities_schools (
  community_id varchar(26) REFERENCES communities(id) ON DELETE CASCADE,
  school_id varchar(26) REFERENCES schools(id) ON DELETE CASCADE,
  CONSTRAINT unique_community_school UNIQUE (community_id, school_id)
);

DROP TABLE IF EXISTS health_centers CASCADE;

CREATE TABLE health_centers (
  id varchar(26) PRIMARY KEY,
  version timestamp,
  name text,
  country text,
  adm1 text,
  adm2 text,
  adm3 text,
  adm4 text,
  latitude float,
  longitude float,
  altitude float,
  center_type character varying,
  staff_women integer,
  staff_men integer,
  avg_female_patients integer,
  avg_male_patients integer,
  have_water_supply character varying,
  hcchwa float,
  have_toilets boolean,
  hccsan float,
  hcchyg float,
  hcchsh float,
  hcc float,
  score varchar(1),
  image_url text,
  country_code varchar(2),
  code text,
  geom geometry(Point, 4326),
  status text
);

CREATE INDEX health_centers_score_idx ON health_centers (score);

CREATE INDEX health_centers_hcc_idx ON health_centers (hcc);

CREATE INDEX health_centers_country_code_idx ON health_centers (country_code);

CREATE INDEX health_centers_country_idx ON health_centers (country);

CREATE INDEX health_centers_adm1_idx ON health_centers (adm1);

CREATE INDEX health_centers_adm2_idx ON health_centers (adm2);

CREATE INDEX health_centers_adm3_idx ON health_centers (adm3);

CREATE INDEX health_centers_adm4_idx ON health_centers (adm4);

CREATE INDEX health_centers_geom_idx ON health_centers USING GIST (geom);

DROP TABLE IF EXISTS communities_health_centers CASCADE;

CREATE TABLE communities_health_centers (
  community_id varchar(26) REFERENCES communities(id) ON DELETE CASCADE,
  health_center_id varchar(26) REFERENCES health_centers(id) ON DELETE CASCADE,
  CONSTRAINT unique_community_health_center UNIQUE (community_id, health_center_id)
);

DROP TABLE IF EXISTS technical_assistance_providers CASCADE;

CREATE TABLE technical_assistance_providers (
id varchar(26) PRIMARY KEY,
  version timestamp,
  name text,
  country text,
  adm1 text,
  adm2 text,
  adm3 text,
  adm4 text,
  latitude float,
  longitude float,
  altitude float,
  type_tap text,
  phase_provide_tap text,
  tap_should_assist integer,
  patsin float,
  patcap float,
  patcob float,
  patint float,
  pat float,
  score varchar(1),
  service_providers_supported_12m integer,
  image_url text,
  country_code varchar(2),
  code text,
  geom geometry(Point, 4326),
  status text
);


CREATE INDEX tap_score_idx ON technical_assistance_providers (score);

CREATE INDEX tap_pat_idx ON technical_assistance_providers (pat);

CREATE INDEX tap_country_code_idx ON technical_assistance_providers (country_code);

CREATE INDEX tap_country_idx ON technical_assistance_providers (country);

CREATE INDEX tap_adm1_idx ON technical_assistance_providers (adm1);

CREATE INDEX tap_adm2_idx ON technical_assistance_providers (adm2);

CREATE INDEX tap_adm3_idx ON technical_assistance_providers (adm3);

CREATE INDEX tap_adm4_idx ON technical_assistance_providers (adm4);

CREATE INDEX tap_geom_idx ON technical_assistance_providers USING GIST (geom);

CREATE OR REPLACE FUNCTION color(value CHAR) 
RETURNS TEXT AS $$
BEGIN
  RETURN CASE value
    WHEN 'A' THEN '#54BA46'
    WHEN 'B' THEN '#FFFF39'
    WHEN 'C' THEN '#FF9326'
    WHEN 'D' THEN '#C92429'
    ELSE '#AAAAAA'
  END;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION color(value float) 
RETURNS TEXT AS $$
BEGIN
  RETURN color(score(value));
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION score(value float) 
RETURNS CHAR AS $$
BEGIN
  RETURN CASE
    WHEN value IS NULL THEN NULL
    WHEN value < 0.4 THEN 'D'
    WHEN value < 0.7 THEN 'C'
    WHEN value < 0.9 THEN 'B'
    ELSE 'A'
  END;
END;
$$ LANGUAGE plpgsql;