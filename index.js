import pino from "pino";
import Output from "./output.js";
import Input from "./input.js";
import countries from "./data/countries_full.json" with { type: "json" };

const logger = pino({
  transport: {
    target: "pino-pretty",
    options: {
      colorize: true,
      colorizeObjects: true,
    },
  },
});

const output = new Output({
  host: process.env.POSTGRES_HOST,
  port: process.env.POSTGRES_PORT,
  user: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB,
});

const siasarCountries = ["BO", "BR", "CO", "CR", "HN", "KG", "MX", "NI", "PA", "PY", "PE", "DO", "SZ"];

const errors = [];

const processCountry = (country) => {
  const input = new Input(country);

  logger.info(`${country.name}: Connecting server`);

  return input
    .connect()
    .then(() => {
      logger.info(`${country.name}: Fetching Points`);
      return input.getPoints();
    })
    .then((points) => {
      if (!points.length) throw new Error("NORECORDS");
      logger.info(`${country.name}: Importing ${points.length} Points`);
      return output.insertPoints(points).then(() => {
        return points;
      });
    })
    .then((points) => {
      logger.info(`${country.name}: Fetching Points data`);
      const pointsIds = points.map((point) => point.id);

      return Promise.all([
        input.getCommunities(pointsIds),
        input.getSystems(pointsIds),
        input.getServiceProviders(pointsIds),
        input.getSchools(),
        input.getHealthCenters(),
        input.getTechnicalAssitanceProviders(),
      ]);
    })
    .then(([communities, systems, providers, schools, healthCenters, taps]) => {
      const inserts = [];

      if (communities.length) {
        logger.info(`${country.name}: Importing ${communities.length} Communities`);
        inserts.push(output.insertCommunities(communities));
      }

      if (systems.length) {
        logger.info(`${country.name}: Importing ${systems.length} Systems`);
        inserts.push(output.insertSystems(systems));
      }

      if (providers.length) {
        logger.info(`${country.name}: Importing ${providers.length} ServiceProviders`);
        inserts.push(output.insertProviders(providers));
      }

      if (schools.length) {
        logger.info(`${country.name}: Adding ${schools.length} Schools`);
        inserts.push(output.insertSchools(schools));
      }

      if (healthCenters.length) {
        logger.info(`${country.name}: Adding ${healthCenters.length} HealthCenters`);
        inserts.push(output.insertHealthCenters(healthCenters));
      }

      if (taps.length) {
        logger.info(`${country.name}: Adding ${taps.length} Technical Assistance Providers`);
        inserts.push(output.insertTechnicalAssistanceProviders(taps));
      }

      return Promise.all(inserts);
    })
    .then(() => {
      logger.info(`${country.name}: Fetching relationships`);
      return Promise.all([
        input.getCommunitiesSystems(),
        input.getCommunitiesSchools(),
        input.getCommunitiesHealthCenters(),
      ]);
    })
    .then(([communitiesSystems, communitiesSchools, communitiesHealthCenters]) => {
      const inserts = [];

      if (communitiesSystems.length) {
        logger.info(`${country.name}: Adding ${communitiesSystems.length} Communities/Systems/Providers relations`);
        inserts.push(output.insertCommunitiesSystems(communitiesSystems));
      }

      if (communitiesSchools.length) {
        logger.info(`${country.name}: Adding ${communitiesSchools.length} Communities/Schools relations`);
        inserts.push(output.insertCommunitiesSchools(communitiesSchools));
      }

      if (communitiesHealthCenters.length) {
        logger.info(`${country.name}: Adding ${communitiesHealthCenters.length} Communities/HealthCenters relations`);
        inserts.push(output.insertCommunitiesHealthCenters(communitiesHealthCenters));
      }

      return Promise.all(inserts);
    })
    .then(() => {
      errors.push(...input.errors);
      logger.info(`${country.name}: Closing input connection`);
      return input.end();
    })
    .catch((err) => {
      if (err.message === "NORECORDS") {
        logger.info(`${country.name}: No records found`);
        return input.end();
      } else {
        logger.error(err);
      }
      return input.end();
    });
};

logger.info("Creating Database Schema");

output
  .createSchema()
  .then(() => {
    logger.info("Getting countries");
    return Object.values(countries.features).map((feature) => ({
      code: feature.properties.ISO_A2,
      name: feature.properties.NAME,
      fullname: feature.properties.ADMIN,
      geom: feature.geometry,
      enabled: siasarCountries.includes(feature.properties.ISO_A2),
    }));
  })
  .then((countries) => {
    logger.info(`Importing ${countries.length} countries`);
    return output.insertCountries(countries).then(() => countries);
  })
  .then((countries) => {
    return Promise.all(
      countries
        .filter((country) => process.env.COUNTRIES.split(",").includes(country.code))
        .map((country) => processCountry(country)),
    );
  })
  .then(() => {
    if (errors.length) {
      logger.warn(`Data Errors\n${errors.map((i) => `${i.country} ${i.entity} ${i.id} ${i.type}`).join("\n")}`);
    }
    logger.info("All Done! Closing output connection");
    return output.end();
  })
  .catch((err) => {
    logger.error(err);
    output.end();
  });
